FROM rockylinux/rockylinux:8.6

# Setup argument default and enviroment variables
ARG WEBUSERNAME=rockyuser

ENV container docker
ENV WEBUSERNAME=${WEBUSERNAME}
ENV WEBUSERADMIN=administrator
ENV DISPLAY=:1
ENV GEOMETRY=1320x720
ENV HOME=/home/${WEBUSERNAME}

RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

# Update the package manager and upgrade the system
# #################################################
RUN yum -y install epel-release
RUN yum -y install 'dnf-command(config-manager)'
RUN yum -y install dnf-plugins-core
RUN yum -y config-manager --set-enabled powertools
RUN yum -y update

RUN yum -y install net-tools lsof passwd bzip2 sudo wget which vim nano
RUN yum -y install openssh-server openssh-clients openssl-devel
RUN yum -y install langpacks-en glibc-langpack-en
RUN yum -y install git make automake autoconf
RUN yum -y install tcl tcllib tcltls tcl-tclxml tcl-doc tcl-devel
RUN yum -y install tk gitk libcurl-devel libxml2-devel libssh-devel libxml2-devel
RUN yum -y install python3.9 python39-devel python39-numpy python39-tkinter
RUN alternatives --set python3 /usr/bin/python3.9

RUN ssh-keygen -A

# Set locale
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8


# Install icewm and tightvnc server.
# #################################################
RUN yum -y install icewm-minimal-session
RUN yum -y install xterm firefox 
RUN yum -y install tigervnc-server 
RUN /bin/dbus-uuidgen > /etc/machine-id


# install and setup noVNC
# #################################################
RUN yum -y install python3-websockify
RUN yum -y install novnc 

RUN yum clean all


# Set up User ${WEBUSERADMIN}
# #################################################
RUN useradd -u 1000 -U -s /bin/bash -m -b /home ${WEBUSERADMIN}
RUN echo "${WEBUSERADMIN}  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/${WEBUSERADMIN}
RUN mkdir /home/${WEBUSERADMIN}/.ssh
COPY authorized_keys /home/${WEBUSERADMIN}/.ssh/authorized_keys
COPY ./local/selfpem /home/${WEBUSERADMIN}/.ssh/selfpem
COPY ./local/certpem /home/${WEBUSERADMIN}/.ssh/certpem
COPY ./local/keypem /home/${WEBUSERADMIN}/.ssh/certpem
RUN chmod -R go-rwx /home/${WEBUSERADMIN}/.ssh
RUN chown -R ${WEBUSERADMIN}:${WEBUSERADMIN} /home/${WEBUSERADMIN}


# Set up User (${WEBUSERNAME})
# #################################################
RUN groupadd -g 59555 secshost
RUN useradd -u 7700 -g 59555 -s /bin/bash -m secshost_adm
RUN useradd -u 1094 -g 59555 -s /bin/bash -m ${WEBUSERNAME}
COPY ./local/dot-bashrc /home/${WEBUSERNAME}/.bashrc
RUN mkdir /home/${WEBUSERNAME}/.ssh
COPY authorized_keys /home/${WEBUSERNAME}/.ssh/authorized_keys
RUN chmod -R go-rwx /home/${WEBUSERNAME}/.ssh
RUN touch /home/${WEBUSERNAME}/.Xauthority
RUN chmod go-rwx /home/${WEBUSERNAME}/.Xauthority
RUN mkdir -p /home/${WEBUSERNAME}/.vnc
RUN mkdir -p /home/${WEBUSERNAME}/.vnc/passwd.cm
COPY ./local/passwd /home/${WEBUSERNAME}/.vnc/passwd.cm
RUN chmod go-rwx /home/${WEBUSERNAME}/.vnc/passwd.cm/passwd
RUN ln -fs /home/${WEBUSERNAME}/.vnc/passwd.cm/passwd /home/${WEBUSERNAME}/.vnc/passwd
RUN mkdir /home/${WEBUSERNAME}/.icewm
COPY ./webuser/dot-icewm/ /home/${WEBUSERNAME}/.icewm/
RUN chown -R 1094:59555 /home/${WEBUSERNAME}
RUN usermod -a -G adm ${WEBUSERADMIN}

# Systemd init
# #################################################
RUN systemctl enable sshd.service


# Finalize installation and default command
# #################################################
RUN rm -f /run/nologin
RUN mkdir /tmp/.X11-unix
RUN chmod 1777 /tmp/.X11-unix

# Expose ports.
EXPOSE 22
EXPOSE 443

WORKDIR /home/${WEBUSERNAME}
VOLUME [ "/sys/fs/cgroup" ]

# Define default command
CMD ["/usr/sbin/init"]
